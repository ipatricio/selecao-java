package com.italopatricio.indra.challenge.prices.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserUnitTest {

    @Test
    public void givenUser_whenGetName_thenReturnNotNull(){
        // given
        User user1 = new User(1,"Usuario A", "usuario", "123");

        // when
        String name = user1.getName();
        String login = user1.getLogin();

        // then
        assertEquals("Usuario A", name);
        assertEquals("usuario", login);
    }
 }
