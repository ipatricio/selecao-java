package com.italopatricio.indra.challenge.prices.services;

import com.italopatricio.indra.challenge.prices.models.PriceHistory;
import com.italopatricio.indra.challenge.prices.repositories.IPriceHistoryRepository;
import com.italopatricio.indra.challenge.prices.services.impl.PriceHistoryServiceImpl;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CsvIntegrationTest {

    @Autowired
    private IPriceHistoryRepository repository;

    @Before
    public void setUp() {

    }

    @Test
    public void givenFileCsv_WhenParse_ThenReturnListPriceHistory() throws IOException, URISyntaxException, ParseException {
        Path path = Paths.get(
                ClassLoader.getSystemResource("2018-1_CA_EXPRESS.csv").toURI());


        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

        BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        CSVReader reader = new CSVReaderBuilder(br).withSkipLines(1).withCSVParser(parser).build();

        List<String[]> rows = reader.readAll();

        List<PriceHistory> priceHistories = new ArrayList<>();
        for (String[] row : rows) {
            if (row.length == 11) priceHistories.add(PriceHistory.getInstancePriceHistory(row));
        }

        repository.saveAll(priceHistories);

        long count = repository.count();

        Assertions.assertThat(count).isGreaterThan(0);
    }

    @Test
    public void importCsvStatic() throws Exception {
//        List result = priceHistoryService.csvImportStatic("2018-1_CA_EXPRESS");
//        Assertions.assertThat(result.size()).isGreaterThan(0);
    }
}
