package com.italopatricio.indra.challenge.prices.services;

import com.italopatricio.indra.challenge.prices.models.User;
import com.italopatricio.indra.challenge.prices.repositories.IUserRepository;
import com.italopatricio.indra.challenge.prices.services.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceIntegrationTest {

    @Autowired
    private IUserService userService;

    @MockBean
    private IUserRepository userRepository;

    @Before
    public void setUp() {
    }

    @Test
    public void givenUser_whenFindById_thenReturnNotNull(){
        // given
        User user = new User(null, "Usuario A","usuarioa","1234");

        userService.create(user);

        // when
        User userFounded = userService.findById(1);

        // then
        assertEquals("usuarioa", userFounded.getLogin());
    }
}
