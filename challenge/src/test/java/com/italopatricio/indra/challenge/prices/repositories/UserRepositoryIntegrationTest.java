package com.italopatricio.indra.challenge.prices.repositories;


import com.italopatricio.indra.challenge.prices.models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private IUserRepository userRepository;

    @Test
    public void givenUser_whenFindAll_thenReturnNotNull() {

        // given
        User user1 = new User(null,"Usuário A", "usuarioa", "1234");

        userRepository.saveAll(Arrays.asList(user1));

        // when
        List<User> users = userRepository.findAll();

        // then
        assertEquals(1, users.size());
    }

    @Test
    public void givenUser_whenFindById_thenReturnNotNull() {

        // given
        User user1 = new User(null,"Usuário A", "usuarioa", "1234");

        userRepository.saveAll(Arrays.asList(user1));

        // when
        User user = userRepository.findById(1).orElse(null);

        // then
        assertEquals("usuarioa", user.getLogin());
    }
}
