package com.italopatricio.indra.challenge.prices.services;

import com.italopatricio.indra.challenge.core.services.ICrudService;
import com.italopatricio.indra.challenge.prices.models.User;
import org.springframework.stereotype.Service;


public interface IUserService extends ICrudService<User, Integer> {
}
