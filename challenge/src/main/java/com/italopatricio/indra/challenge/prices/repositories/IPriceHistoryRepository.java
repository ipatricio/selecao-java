package com.italopatricio.indra.challenge.prices.repositories;

import com.italopatricio.indra.challenge.prices.models.PriceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IPriceHistoryRepository extends JpaRepository<PriceHistory, Integer> {

    @Query(nativeQuery = true, value = "SELECT ph.county, AVG(ph.salePrice) as salePriceAvg, AVG(ph.purchasePrice) as purchasePriceAvg " +
            "FROM priceHistory ph GROUP BY city")
    List<?> groupAverageByCity();

}
