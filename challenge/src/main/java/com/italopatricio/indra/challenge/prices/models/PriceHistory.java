package com.italopatricio.indra.challenge.prices.models;


import com.italopatricio.indra.challenge.prices.enums.PosPriceHistory;
import com.italopatricio.indra.challenge.prices.utils.Utils;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvNumber;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.beanutils.locale.converters.DateLocaleConverter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.swing.text.DateFormatter;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class PriceHistory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String regionAcronym;

    private String stateAcronym;

    private String county;

    private String resale;

    private String installationCode;

    private String product;

    private Date dateOfCollect;

    private Double purchasePrice;

    private Double salePrice;

    private String unitOfMeasurement;

    private String flag;

    public static PriceHistory getInstancePriceHistory(String[] inputs) throws ParseException {
        PriceHistory newInstance = new PriceHistory();

        newInstance.setRegionAcronym(inputs[PosPriceHistory.REGION_ACRONYM.getCod()]);
        newInstance.setStateAcronym(inputs[PosPriceHistory.STATE_ACRONYM.getCod()]);
        newInstance.setCounty(inputs[PosPriceHistory.COUNTY.getCod()]);
        newInstance.setResale(inputs[PosPriceHistory.RESALE.getCod()]);
        newInstance.setInstallationCode(inputs[PosPriceHistory.INSTALLATION_CODE.getCod()]);
        newInstance.setProduct(inputs[PosPriceHistory.PRODUCT.getCod()]);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        newInstance.setDateOfCollect(sdf.parse(inputs[PosPriceHistory.DATE_OF_COLLECT.getCod()]));


        NumberFormat format = NumberFormat.getInstance(Locale.GERMANY);

        if(!inputs[PosPriceHistory.PURCHASE_PRICE.getCod()].isEmpty())
            newInstance.setPurchasePrice(format.parse(inputs[PosPriceHistory.PURCHASE_PRICE.getCod()]).doubleValue());

        if(!inputs[PosPriceHistory.SALE_PRICE.getCod()].isEmpty())
            newInstance.setSalePrice(format.parse(inputs[PosPriceHistory.SALE_PRICE.getCod()]).doubleValue());

        newInstance.setUnitOfMeasurement(inputs[PosPriceHistory.UNIT_OF_MEASUREMENT.getCod()]);
        newInstance.setFlag(inputs[PosPriceHistory.FLAG.getCod()]);


        return newInstance;
    }
}
