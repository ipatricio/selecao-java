package com.italopatricio.indra.challenge.prices.services.impl;


import com.italopatricio.indra.challenge.core.services.impl.AbstractCrudService;
import com.italopatricio.indra.challenge.prices.models.User;
import com.italopatricio.indra.challenge.prices.repositories.IUserRepository;
import com.italopatricio.indra.challenge.prices.services.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserServiceImpl extends AbstractCrudService<User, Integer> implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    protected PagingAndSortingRepository getRepository() {
        return userRepository;
    }
}
