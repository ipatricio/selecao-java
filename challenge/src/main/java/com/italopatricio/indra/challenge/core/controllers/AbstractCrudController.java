package com.italopatricio.indra.challenge.core.controllers;

import com.italopatricio.indra.challenge.core.services.ICrudService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.io.Serializable;
import java.util.*;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractCrudController<T extends Serializable, ID> {

    private final ICrudService<T, ID> service;


    public ICrudService<T, ID> getService() {
        return service;
    }

    @GetMapping
    public List<T> listAll() {
        return this.service.findAll();
    }

    @PostMapping
    public Map<String, Object> create(@RequestBody T json) {
        log.debug("create() with body {} of type {}", json, json.getClass());

        T created = this.service.create(json);

        return Map.of("success", true,
                "created", created);
    }

    @GetMapping("/{id}")
    public T getById(@PathVariable ID id) {
        return this.service.findById(id);
    }

    @PatchMapping("/{id}")
    public Map<String, Object> update(@PathVariable ID id, @RequestBody T json) {
        log.debug("update() of id#{} with body {}", id, json);
        log.debug("T json is of type {}", json.getClass());

        T entity = this.service.findById(id);
        try {
            BeanUtils.copyProperties(json, entity);
        } catch (Exception e) {
            log.warn("while copying properties", e);
            throw e;
        }

        log.debug("merged entity: {}", entity);
        T updated = this.service.update(entity);
        log.debug("updated enitity: {}", updated);

        return Map.of(
                "success", true,
                "id", id,
                "updated", updated
        );
    }

    @DeleteMapping("/{id}")
    public Map<String, Object> delete(@PathVariable ID id) {
        this.service.deleteById(id);
        return Map.of("success", true);
    }
}
