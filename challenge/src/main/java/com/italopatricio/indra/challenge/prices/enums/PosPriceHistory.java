package com.italopatricio.indra.challenge.prices.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
public enum PosPriceHistory {

     REGION_ACRONYM(0),
     STATE_ACRONYM(1),
     COUNTY(2),
     RESALE(3),
     INSTALLATION_CODE(4),
     PRODUCT(5),
     DATE_OF_COLLECT(6),
     PURCHASE_PRICE(7),
     SALE_PRICE(8),
     UNIT_OF_MEASUREMENT(9),
     FLAG(10);

     @Getter
     private Integer cod;



}
