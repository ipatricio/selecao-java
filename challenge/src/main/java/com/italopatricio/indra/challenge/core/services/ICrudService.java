package com.italopatricio.indra.challenge.core.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

public interface ICrudService<T extends Serializable, K> {

    T findById(final K id);

    List<T> findAll();

    Page<T> findPage(Pageable pageable);

    T create(final T entity);

    T update(final T entity);

    void delete(final T entity);

    void deleteById(final K entityId);

}
