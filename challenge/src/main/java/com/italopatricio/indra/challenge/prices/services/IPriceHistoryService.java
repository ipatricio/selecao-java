package com.italopatricio.indra.challenge.prices.services;

import com.italopatricio.indra.challenge.core.services.ICrudService;
import com.italopatricio.indra.challenge.prices.models.PriceHistory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;


public interface IPriceHistoryService extends ICrudService<PriceHistory, Integer> {
    public List<PriceHistory> csvImport(MultipartFile fileName) throws Exception;
    public List<PriceHistory> csvImportStatic(String fileName) throws Exception;
}
