package com.italopatricio.indra.challenge.prices.repositories;

import com.italopatricio.indra.challenge.prices.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {
}
