package com.italopatricio.indra.challenge.prices.services.impl;

import com.italopatricio.indra.challenge.core.services.impl.AbstractCrudService;
import com.italopatricio.indra.challenge.prices.models.PriceHistory;
import com.italopatricio.indra.challenge.prices.repositories.IPriceHistoryRepository;
import com.italopatricio.indra.challenge.prices.services.IPriceHistoryService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@Service
public class PriceHistoryServiceImpl extends AbstractCrudService<PriceHistory, Integer> implements IPriceHistoryService {

    private IPriceHistoryRepository priceHistoryRepository;

    public PriceHistoryServiceImpl(IPriceHistoryRepository priceHistoryRepository){
        this.priceHistoryRepository = priceHistoryRepository;
    }



    public List<PriceHistory> csvImport(MultipartFile multipartFile) throws Exception {
        List<String[]> rows = readFile(multipartFile);

        List<PriceHistory> priceHistories = new ArrayList<>();
        for (String[] row : rows) {
            if(row.length == 11) priceHistories.add(PriceHistory.getInstancePriceHistory(row));
        }

        return priceHistoryRepository.saveAll(priceHistories);
    }

    @Override
    public List<PriceHistory> csvImportStatic(String fileName) throws Exception {
        Path path = Paths.get(
                ClassLoader.getSystemResource(fileName).toURI());

        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

        BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        CSVReader reader = new CSVReaderBuilder(br).withSkipLines(1).withCSVParser(parser).build();

        List<String[]> rows = reader.readAll();

        List<PriceHistory> priceHistories = new ArrayList<>();
        for (String[] row : rows) {
            if(row.length == 11) priceHistories.add(PriceHistory.getInstancePriceHistory(row));
        }

        return priceHistoryRepository.saveAll(priceHistories);
    }


    public List<String[]> readFile(MultipartFile multipartFile) throws Exception {
        File file = convertToFile(multipartFile);
        CSVReaderBuilder builder = new CSVReaderBuilder(new FileReader(file));
        CSVReader csvReader = builder.withCSVParser(
                                    new CSVParserBuilder().withSeparator(';').build())
                            .withSkipLines(1).build();
        return csvReader.readAll();
    }

    public static File convertToFile(MultipartFile multipartFile) throws IOException {
        File file1 = new File(multipartFile.getOriginalFilename());
        FileOutputStream fileOutputStream = new FileOutputStream(file1);
        fileOutputStream.write(multipartFile.getBytes());
        fileOutputStream.close();

        return file1;
    }

    @Override
    protected PagingAndSortingRepository<PriceHistory, Integer> getRepository() {
        return priceHistoryRepository;
    }
}
