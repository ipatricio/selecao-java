package com.italopatricio.indra.challenge.prices.controllers;

import com.italopatricio.indra.challenge.core.controllers.AbstractCrudController;
import com.italopatricio.indra.challenge.core.services.ICrudService;
import com.italopatricio.indra.challenge.prices.models.PriceHistory;
import com.italopatricio.indra.challenge.prices.repositories.IPriceHistoryRepository;
import com.italopatricio.indra.challenge.prices.services.IPriceHistoryService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@RestController
@RequestMapping("/api/price-history")
public class PriceHistoryController extends AbstractCrudController<PriceHistory, Integer> {

    private IPriceHistoryService priceHistoryService;

    private IPriceHistoryRepository priceHistoryRepository;

    public PriceHistoryController(IPriceHistoryService priceHistoryService) {
        super(priceHistoryService);
        this.priceHistoryService = priceHistoryService;
    }

    @PostMapping("/import-static")
    public ResponseEntity<?> importCsvStatic() throws Exception{
        Path path = Paths.get(
                ClassLoader.getSystemResource("2018-1_CA_EXPRESS.csv").toURI());


        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

        BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        CSVReader reader = new CSVReaderBuilder(br).withSkipLines(1).withCSVParser(parser).build();

        List<String[]> rows = reader.readAll();

        List<PriceHistory> priceHistories = new ArrayList<>();
        for (String[] row : rows) {
            if (row.length == 11) priceHistories.add(PriceHistory.getInstancePriceHistory(row));
        }

        priceHistoryRepository.saveAll(priceHistories);

        long count = priceHistoryRepository.count();

        if(count > 0) {
            return ResponseEntity.ok().body(Map.of(
                    "sucess", true,
                    "message", "Importação concluída com sucesso!"
            ));
        }

        return ResponseEntity.ok().body(Map.of(
                "sucess", false,
                "message", "Importação falhou!"
        ));
    }

    @PostMapping(value = "/import", consumes = {"multipart/form-data"})
    public ResponseEntity<?> importCsv(@RequestPart("file") MultipartFile file) throws Exception {
            String ext = FilenameUtils.getExtension(file.getOriginalFilename());
            if (Objects.nonNull(ext) && !Objects.equals(ext,"csv")) {

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                        "success", false,
                        "message", "Arquivo de extensão inválida!"
                ));
            }
            long size = priceHistoryService.csvImport(file).size();
            if (size > 0) {
                return ResponseEntity.ok().body(Map.of(
                        "success", true,
                        "message","Arquivo importado com sucesso!"
                ));
            }

            return ResponseEntity.status(500).body(Map.of(
                "success", false,
                "message", "Falha ao processar arquivo!"
            ));
    }


    @ApiOperation(value = "Média de preço de combustível com base no nome do município.", httpMethod = "GET")
    @GetMapping("/average-by-city")
    public ResponseEntity<?> averageByCity(@RequestParam String county){
       return null;
    }



}
