package com.italopatricio.indra.challenge.prices.controllers;

import com.italopatricio.indra.challenge.core.controllers.AbstractCrudController;
import com.italopatricio.indra.challenge.prices.models.User;
import com.italopatricio.indra.challenge.prices.services.IUserService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController extends AbstractCrudController<User, Integer> {

    private final IUserService userService;

    public UserController(IUserService userService) {
        super(userService);
        this.userService = userService;
    }

    @Override
    @ApiOperation(value="Listar todos os usuários")
    public List<User> listAll() {
        return super.listAll();
    }

    @Override
    @ApiOperation(value="Cadastrar usuário")
    public Map<String, Object> create(@RequestBody User json) {
        return super.create(json);
    }

    @Override
    @ApiOperation(value="Buscar usuário por ID")
    public User getById(@PathVariable Integer id) {
        return super.getById(id);
    }

    @Override
    @ApiOperation(value="Atualizar usuário")
    public Map<String, Object> update(@PathVariable Integer id,@RequestBody User json) {
        return super.update(id, json);
    }

    @Override
    @ApiOperation(value="Remover usuário por ID")
    public Map<String, Object> delete(@PathVariable Integer id) {
        return super.delete(id);
    }
}
