package com.italopatricio.indra.challenge.core.services.impl;

import com.italopatricio.indra.challenge.core.services.ICrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;


import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.google.common.collect.Lists;

@Transactional
public abstract class AbstractCrudService<T extends Serializable, K> implements ICrudService<T, K> {

    // read - one

    @Override
    @Transactional(readOnly = true)
    public T findById(final K id) {
        return getRepository().findById(id).orElse(null);
    }

    // read - all

    @Override
    @Transactional(readOnly = true)
    public List<T> findAll() {
        return Lists.newArrayList(getRepository().findAll());
    }

    @Override
    public Page<T> findPage(Pageable pageable) { return getRepository().findAll(pageable); }

    // write

    @Override
    public T create(final T entity) {
        return getRepository().save(entity);
    }

    @Override
    public T update(final T entity) {
        return getRepository().save(entity);
    }
    
    @Override
    public void delete(T entity) {
        getRepository().delete(entity);
    }
    
    @Override
    public void deleteById(K entityId) {
        T entity = findById(entityId);
        delete(entity);
    }

    protected abstract PagingAndSortingRepository<T, K> getRepository();

}
